package voicechat;

import java.io.*;
import java.util.Calendar;

import javax.sound.sampled.*;

public class VoiceRecorder {
	
	/**
	 * Record a voice message and write it
	 * into a file in the voice/ directory.
	 */

	private File recordFile;
	private String recordName;
	private boolean recording = false;
	private AudioFormat soundFormat;
	private AudioFileFormat.Type extension;
	private TargetDataLine micLine;

	
	public VoiceRecorder() {
		recordFile = new File("");
		
		try {
			setupAudioFormat();
		}
		catch (LineUnavailableException ex) {
			System.err.println("Setup error..");
			ex.printStackTrace();
		}
	}
	
	private void setupAudioFormat() throws LineUnavailableException {
		extension = AudioFileFormat.Type.WAVE;
	    soundFormat = new AudioFormat(16000, 8, 2, true, true);
	    
	    DataLine.Info outputInfo = new DataLine.Info
	    	(TargetDataLine.class, soundFormat);
	    
	    // get the microphone line
		micLine = (TargetDataLine) AudioSystem.getLine(outputInfo);
	}
	
	private void constructRecordFile() {

		String fileName = VoiceRecorder.getFileNameFormat();
		
		recordName = fileName;
		recordFile = new File("voice/" + recordName);
		recordFile.getParentFile().mkdirs();
	}
	
	public static String getFileNameFormat() {
		Calendar now = Calendar.getInstance();
		
		return String.format("rec%s%s%s%s%s%s.wav", 
				now.get(Calendar.DAY_OF_MONTH), now.get(Calendar.MONTH) + 1, 
				now.get(Calendar.YEAR), now.get(Calendar.HOUR_OF_DAY), 
				now.get(Calendar.MINUTE), now.get(Calendar.SECOND));
	}
	
	public void startRecording() throws IOException, LineUnavailableException {
	
		this.recording = true;
		constructRecordFile();
		
		// open the microphone line using format
		micLine.open(soundFormat);

		// start receiving input 
		micLine.start();	
		AudioInputStream audioIn = new AudioInputStream(micLine);
		
		// write voice input in file
		AudioSystem.write(audioIn, extension, recordFile);
	}
	
	public void stopRecording() {
	
		this.recording = false;
		
		micLine.stop();
		micLine.close();
	}
	
	public boolean isRecording() {
		return recording;
	}
	
	public File getRecordFile() {
		return recordFile;
	}

	public String getRecordName() {
		return recordFile.getName();
	}
	
}
