package voicechat;

import java.io.Serializable;

public class VoiceDetails implements Serializable {
	
	/**
	 * Store voice message details.
	 */

	private static final long serialVersionUID = 1L;

	private String sender;
	private int fileSize;
	
	public VoiceDetails(String sender, int fileSize) {
		this.sender = sender;
		this.fileSize = fileSize;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}
	
}