package voicechat;

import java.net.*;
import javax.swing.JOptionPane;

import main.Util;

import java.io.*;

public class VoiceReceiver extends Thread {
	
	/**
	 * Listen to a port at the client, in order to
	 * receive voice messages from another client and
	 * play them.
	 */
	
	public static final int VOICE_CHAT_PORT = 8888;
	
	private ServerSocket listener;
	private String sender;
	private int voiceSize;
	private int batchSize;
	private int actualBatch;
	
	@Override
	public void run() {
		try {
			listener = new ServerSocket(VOICE_CHAT_PORT);
			
			while (true) {
				try {
					receive();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			Util.promptError(e);
		}
	}
	
	public void receive() throws Exception {
		
		switch(listenToHandshake()) {
			case AT_ONCE:
				receiveAtOnce();
				break;
			case BATCH:
				receiveByBatch();
				break;
		}
	}
	
	
	private SendType listenToHandshake()
		throws IOException, ClassNotFoundException {

		System.out.println
			("VoiceReceiver server is listening at "+VOICE_CHAT_PORT+"...");

		Socket socket = listener.accept();
		
		ObjectInputStream sizeReceiver =
			new ObjectInputStream(socket.getInputStream());

		VoiceDetails voiceDetails = (VoiceDetails)
			sizeReceiver.readObject();
		
		sender = voiceDetails.getSender();
		voiceSize = voiceDetails.getFileSize();
		batchSize = SendType.checkAmountOfBatches(voiceSize);
		
		System.out.println("Packet size is : " + voiceSize); 
		System.out.println("Object read");
		System.out.println("Batch size " + batchSize);
		System.out.println("Voice size " + voiceSize);
		
		socket.close();
		
		return SendType.checkSendType(voiceSize);
	}


	private void receiveAtOnce() throws IOException {

		DatagramSocket receiver = new DatagramSocket
			(VOICE_CHAT_PORT);
		DatagramPacket voicePacket = new DatagramPacket
			(new byte[voiceSize], voiceSize);
		
		receiver.receive(voicePacket);
		System.out.println
			("Voice packet received, length " + voicePacket.getLength());
		
		receiver.close();
		
		saveThenPlayVoiceFile(voicePacket.getData());
	}
	
	private void saveThenPlayVoiceFile(byte[] voiceBytes)
		throws IOException {

		String fileName = VoiceRecorder.getFileNameFormat();
		String path = "voice/" + fileName;

		// write the file to voice dir
		File file = new File(path);
		file.getParentFile().mkdirs();
		FileOutputStream stream = new FileOutputStream(file);
		stream.write(voiceBytes);
		stream.close();
		
		JOptionPane.showMessageDialog
			(null, "You have received a voice message from: " + sender,
				"Voice Message", JOptionPane.INFORMATION_MESSAGE);
		
		// play the file
		VoicePlayer player = new VoicePlayer();
		player.playSound(path);
	}
	
	private void receiveByBatch() throws SocketException, IOException {
		
		actualBatch = 0;
		
		// create byte batches
		byte[][] voicePackets = new byte[batchSize][SendType.MAX_BATCH_SIZE];
		// full Size voice byte
		byte[] voicePacket = new byte[voiceSize];
		
		DatagramSocket socket = new DatagramSocket(VOICE_CHAT_PORT);
		DatagramPacket packet = new DatagramPacket
			(voicePackets[0], SendType.MAX_BATCH_SIZE);
		
		// receive the packet by batches
		for (int batch = 0; batch < batchSize; batch++) {
			packet.setData(voicePackets[batch]);
			socket.receive(packet);
			actualBatch++;
			System.out.printf("Batch received %d/%d\n", actualBatch, batchSize);
		}
		
		socket.close();
		
		// fill the voicePacket byte from the voicePackets
		// reconstruct voicePacket
		for (int batch = 0; batch < batchSize; batch++) {
			
			for (int block = 0; block < SendType.MAX_BATCH_SIZE; block++) {
				
				if (block + (batch * SendType.MAX_BATCH_SIZE) == voiceSize)
					break;
				
				voicePacket[block + (batch * SendType.MAX_BATCH_SIZE)] =
					voicePackets[batch][block];
			}
		}
		
		saveThenPlayVoiceFile(voicePacket);
	}
	
	public String getSender() {
		return sender;
	}
	
}
