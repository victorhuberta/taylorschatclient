package voicechat;

import javax.sound.sampled.LineUnavailableException;
import java.io.IOException;

public class RecordHandler extends Thread {
	
	/**
	 * Handles a VoiceRecorder object in another thread,
	 * this thread will be used to by the caller either ONLY to start,
	 * or ONLY to stop recording, by calling start();
	 * 
	 * imagine every time this new thread is created and started,
	 * acts as an on/off switch for a voice recorder object.
	 * 
	 * 
	 * tells the thread caller whether the recorder successfully 
	 * recording voice or not.
	 * @author : Daniel Jonathan
	 */
	
	private VoiceRecorder recorder;
	private boolean successful;
	
	/**
	 * Handles the specific recorder object
	 * being passed in the constructor
	 * @param recorder : the recorder object
	 */
	public RecordHandler(VoiceRecorder recorder) {
		this.recorder = recorder;
	}
	
	/**
	 * start recording if the recorder is recording,
	 * otherwise stop.
	 * Set the successful flag to false, if something wrong
	 * happens in the recording process.
	 */
	public void run() {
		
		try {
			if (recorder.isRecording()) {
				recorder.stopRecording();
				this.successful = true;
			}
			else recorder.startRecording();
		}
		catch (LineUnavailableException | IOException ex) {
			System.err.println("Something wrong while recording");
			ex.printStackTrace();
			recorder.stopRecording();
			successful = false;
		}
		
	}
	
	/**
	 * check whether the recording is successful or not,
	 * call this method after running start() , 
	 * to see if the voice file has been successfully 
	 * constructed.
	 */
	public boolean isSuccessful() {
		return successful;
	}
		
}
