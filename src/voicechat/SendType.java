package voicechat;


public enum SendType {
	
	AT_ONCE, BATCH;
	
	/**
	 * Limit : 16KB
	 */
	public final static int MAX_BATCH_SIZE = 4096;
	
	/**
	 * Checks whether the file is larger than 
	 * 16KB, if it is, sent by batches,
	 * otherwise send once.
	 * @param size : size of the file (in bytes)
	 * @return SendType constant, AT_ONCE, or BATCH
	 */
	public static SendType checkSendType(int size) {
		return size <= MAX_BATCH_SIZE ?
			SendType.AT_ONCE : SendType.BATCH; 
	}
	
	/**
	 * Check how many times files need to be
	 * sent to the receiver, or check how many times
	 * the receiver have to receive packet from sender
	 * @param size : size of the file (in bytes)
	 * @return amount of batches to sent/receive
	 */
	public static int checkAmountOfBatches(int size) {
		return (int) Math.ceil
			(size / (double) MAX_BATCH_SIZE);
	}
}
