package voicechat;

import java.io.*;
import java.net.*;
import java.rmi.UnknownHostException;

import main.TaylorsChatClientProgram;

public class VoiceSender {
	
	/**
	 * Connect to the VoiceReceiver server at another
	 * client, and send the voice message.
	 */
	
	public static final int VOICE_CHAT_PORT = 8888;
	
	private static File voiceFile = null;
	
	/**
	 * Send voice chat to a specific address,
	 * The caller must first choose file first,
	 * using chooseFile method 
	 * otherwise, a FileNotFoundException might be thrown
	 * 
	 * @param address
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void sendVoiceChat(File voiceFile, String address) 
		throws FileNotFoundException, IOException, InterruptedException {	
		
		VoiceSender.voiceFile = voiceFile;
		
		switch(performHandshake(address)) {
			case AT_ONCE:
				beginSendAtOnce(address);
				return;
			case BATCH:
				beginSendByBatch(address);
				return;
		}
	}
	
	/**
	 * Perform handshake, in this case
	 * the sender sends the size of the packet to the receiver and
	 * determines whether the delivery should be sent at once,
	 * or batch by batch
	 * 
	 * @param address : address to be sent to 
	 * @return SendType constant
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	private static SendType performHandshake(final String address)
		throws UnknownHostException, IOException {

		int length = (int) voiceFile.length();
		
		System.out.println("length is: " + length);
		
		Socket sender = new Socket(address, VOICE_CHAT_PORT);

		ObjectOutputStream outputStream = new ObjectOutputStream
			(sender.getOutputStream());

		outputStream.writeObject(new VoiceDetails
			(TaylorsChatClientProgram.chatClient
				.getUsername(), length));
		
		sender.close();
		
		return SendType.checkSendType(length);
	}
	

	/**
	 * Sends all voice chat packets at once,
	 * using UDP protocol.
	 *
	 * @param address
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static void beginSendAtOnce(final String address)
		throws IOException, InterruptedException {
		
		// Get the file input stream and read 
		// it to byte array
		FileInputStream inStream = new FileInputStream(voiceFile);
		int length = (int) voiceFile.length();
		
		byte[] voiceData = new byte[length];
		inStream.read(voiceData, 0, length);
		inStream.close();
		
		// begin crafting the voice packet from byte array
		DatagramPacket voicePacket = new DatagramPacket
			(voiceData, voiceData.length,
				InetAddress.getByName(address), 8888);

		DatagramSocket socket = new DatagramSocket();
		
		// ensure the receiver starts calling receive method first
		Thread.sleep(10);
		// sent the voice
		socket.send(voicePacket);
		System.out.println("Voice packet sent");
		
		socket.close();
	}
	
	/**
	 * Sends all voice chat packets batch by batch,
	 * using UDP protocol (1 batch = 4KB).
	 * 
	 * @param address
	 * @throws InterruptedException
	 * @throws IOException
	 */
	private static void beginSendByBatch(final String address)
		throws InterruptedException, IOException {
		
		int length = (int) voiceFile.length();
		int batches = SendType.checkAmountOfBatches(length);
		
		FileInputStream inStream = new FileInputStream(voiceFile);
		byte[][] voicePackets = new byte[batches][SendType.MAX_BATCH_SIZE];
		
		byte[] voicePacket = new byte[length];
		
		inStream.read(voicePacket, 0, length);
		inStream.close();

		// separating packets
		for (int batch = 0; batch < batches; batch++) {
			
			for (int block = 0; block < SendType.MAX_BATCH_SIZE; block++) {
				
				if (block + (batch * SendType.MAX_BATCH_SIZE) == voicePacket.length)
					break;
				
				voicePackets[batch][block] =
					voicePacket[block + (batch * SendType.MAX_BATCH_SIZE)];
			}
		}
		
		DatagramPacket packet = new DatagramPacket
			(voicePacket, length, InetAddress.getByName(address), 8888);

		DatagramSocket socket = new DatagramSocket();

		// send packets by batch
		for (int batch = 0; batch < batches; batch++) {
			Thread.sleep(100);
			packet.setData(voicePackets[batch]);
			packet.setLength(SendType.MAX_BATCH_SIZE);
			socket.send(packet);
		}

		socket.close();
		
	}

}