package security;

import java.io.*;
import java.net.Socket;
import java.security.*;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;

public class ChatIntegrity {
	
	/**
	 * This class handles all encryption and decryption routines.
	 * Notice that the protocol used by this class to perform
	 * the routine is not supposed to be used in real world
	 * distributed applications. There are several serious
	 * issues with the implementation and it is only for
	 * demonstration purposes.
	 */
	
	private static final byte[] key =
		"BC8446BFF39E4FDCCD34D2C99A47FAHB".getBytes(); // Bad practice
	private static final String transformation =
		"AES/ECB/PKCS5PADDING";
	
	/**
	 * This method first encrypt chat object using AES algorithm and
	 * the specified key, then send it to the server.
	 * 
	 * @param obj
	 * @param socket
	 * @return status of a successful send
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws IOException
	 */
	public static boolean encryptAndSend(Serializable obj, Socket socket) 
		throws Exception {

		// Initialize cipher in encrypt mode by specifying
		// key and algorithm.
		SecretKeySpec sks = new SecretKeySpec(key, "AES");
		Cipher cipher = Cipher.getInstance(transformation);

		ObjectOutputStream oos = new ObjectOutputStream
			(socket.getOutputStream());

		cipher.init(Cipher.ENCRYPT_MODE, sks);
		
		try {
			// Write encrypted object to output stream.
			SealedObject sealed = new SealedObject(obj, cipher);
			oos.writeObject(sealed);
			return true;
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * This method receives any chat objects from the server,
	 * decrypt it using AES algorithm and the specified
	 * key, then return it.
	 * 
	 * @param socket
	 * @return the received chat object
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws IOException
	 */
	public static Object recvAndDecrypt(Socket socket) 
		throws Exception {

		// Initialize cipher in decrypt mode by specifying
		// key and algorithm.
		SecretKeySpec sks = new SecretKeySpec(key, "AES");
		Cipher cipher = Cipher.getInstance(transformation);

		ObjectInputStream ois = new ObjectInputStream
			(socket.getInputStream());
		
		cipher.init(Cipher.DECRYPT_MODE, sks);
		
		try {
			// Read encrypted object from output stream
			SealedObject sealed = (SealedObject) ois.readObject();
			return sealed.getObject(cipher);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * The MD5 static method is used for calculating user 
	 * password hashes and authentication tokens.
	 * 
	 * @param str
	 * @return the MD5 hash of the string
	 */
	public static String MD5(String str) {
		try {

	        MessageDigest md = MessageDigest.getInstance("MD5");
	        byte[] arrayOfBytes = md.digest(str.getBytes("UTF-8"));
	        StringBuffer sb = new StringBuffer();
	        
	        for (int i = 0; i < arrayOfBytes.length; i++) {
	        	// Only take the first byte and clear upper bytes.
	        	// Then get second and third bytes.
	        	sb.append(Integer.toHexString
	        		((arrayOfBytes[i] & 0xFF) | 0x100).substring(1, 3));
	        }
	        
	        return sb.toString().toUpperCase();

		} catch (NoSuchAlgorithmException |
			UnsupportedEncodingException e) {

			e.printStackTrace();
			return null;
		}
	}
	
	public static String generateAuthToken() {
		double rand = Math.random() * System.nanoTime() + 1000000;
		return MD5(Double.toString(rand));
	}
	
}