package gui;

import java.awt.*;
import java.awt.event.*;
import java.io.Serializable;
import java.util.*;
import javax.swing.*;

import main.*;
import objects.*;
import security.ChatIntegrity;

public class CommunicationGUI implements ActionListener {
	
	/**
	 * This class displays the current active users list
	 * retrieved from the server. Admin is able to select
	 * any user and chat with them. Admin can also create
	 * a new multicast group and add users to it.
	 */
	
	private static final String CREATE_NEW_GROUP = "Create New Group";
	private static final String EDIT_GROUP = "Edit Group";
	private static final String REMOVE_GROUP = "Remove Group";
	
	private JPanel communicationPanel;
	
	private JScrollPane activeUsersScroll;
	private JList<String> activeUsersList;

	private JComboBox<String> usersGroupsActionsCmbBox;
	private JButton usersGroupsActionsBtn;
	private JScrollPane usersGroupsScroll;
	private JList<String> usersGroupsList;
	private HashMap<String, ArrayList<String>> usersGroups;
	
	private JPanel buttonsPanel;
	private JButton refreshBtn;
	private JButton chatBtn;
	
	private HashMap<String, String> chatHistory;
	
	public CommunicationGUI() {
		communicationPanel = new JPanel();
		
		activeUsersList = new JList<String>();
		activeUsersList.setModel(new DefaultListModel<String>());
		
		usersGroupsActionsCmbBox = new JComboBox<String>
			(new String[]{
				CREATE_NEW_GROUP,
				EDIT_GROUP,
				REMOVE_GROUP
			});
		usersGroupsActionsBtn = new JButton("GO");
		
		usersGroupsList = new JList<String>();
		usersGroupsList.setModel(new DefaultListModel<String>());
		
		usersGroups = new HashMap<String, ArrayList<String>>();
		
		buttonsPanel = new JPanel();
		refreshBtn = new JButton("Refresh");
		chatBtn = new JButton("Chat");
		
		chatHistory = new HashMap<String, String>();
	}
	
	public void init() {
		usersGroupsActionsBtn.addActionListener(this);
		refreshBtn.addActionListener(this);
		chatBtn.addActionListener(this);
		
		initActiveUsersList();
		initButtonsPanel();
		initCommunicationPanel();
	}
	
	private void initActiveUsersList() {
		activeUsersList.setFont(new Font("Dialog", Font.PLAIN, 20));
		activeUsersList.setBackground(new Color(240, 240, 255));
		activeUsersScroll = getScrollPaneOf(activeUsersList);
		activeUsersScroll.setPreferredSize(new Dimension(0, 180));
	}
	
	private JScrollPane getScrollPaneOf(JList<String> list) {
		return new JScrollPane
			(list, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	}
	
	private void initButtonsPanel() {
		buttonsPanel.setLayout(new BoxLayout
			(buttonsPanel, BoxLayout.LINE_AXIS));
		buttonsPanel.add(refreshBtn);
		buttonsPanel.add(Box.createHorizontalGlue());
		buttonsPanel.add(chatBtn);
	}
	
	private void initCommunicationPanel() {
		communicationPanel.setLayout(new BoxLayout
			(communicationPanel, BoxLayout.PAGE_AXIS));
		communicationPanel.add(activeUsersScroll);
		
		initUsersGroupsActionsPanel();
		initUsersGroupsList();

		communicationPanel.add(buttonsPanel);		
	}
	
	private void initUsersGroupsActionsPanel() {
		JPanel usersGroupsActionsPanel = new JPanel();
		
		usersGroupsActionsPanel.setLayout
			(new BoxLayout(usersGroupsActionsPanel, BoxLayout.LINE_AXIS));

		usersGroupsActionsPanel.setPreferredSize(new Dimension(0, 10));
		usersGroupsActionsPanel.add(new JLabel("Users Groups:"));
		usersGroupsActionsPanel.add(Box.createHorizontalGlue());
		usersGroupsActionsPanel.add(usersGroupsActionsCmbBox);
		usersGroupsActionsPanel.add(usersGroupsActionsBtn);
		
		communicationPanel.add(usersGroupsActionsPanel);
	}
	
	private void initUsersGroupsList() {
		usersGroupsList.setFont(new Font("Dialog", Font.PLAIN, 20));
		usersGroupsList.setBackground(new Color(240, 240, 255));

		usersGroupsScroll = getScrollPaneOf(usersGroupsList);
		usersGroupsScroll.setPreferredSize(new Dimension(0, 150));
		
		communicationPanel.add(usersGroupsScroll);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == refreshBtn) {
			refreshEventCall();
		} else if (e.getSource() == chatBtn) {
			chatEventCall();
		} else if (e.getSource() == usersGroupsActionsBtn) {
			String selectedAction = (String)
				usersGroupsActionsCmbBox.getSelectedItem();

			usersGroupsActionsEventCall(selectedAction);
		}
	}
	
	public void refreshEventCall() {
		if (rejectIfNotAdmin()) return;
		
		try {
			getActiveUsersListFromServer();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * A precaution in case a normal user
	 * is trying to use an admin-specific feature.
	 *
	 * @return reject status
	 */
	public boolean rejectIfNotAdmin() {
		if (! TaylorsChatClientProgram.chatClient
				.getUsername().equals("admin")) {

			JOptionPane.showMessageDialog
			(
				null, 
				"You can't use this feature. You are not an admin.", 
				"Invalid Permission",
				JOptionPane.ERROR_MESSAGE
			);
			return true;
		} else return false;
	}
	
	public void getActiveUsersListFromServer() throws Exception {
		if (TaylorsChatClientProgram.chatClient == null) return;
		
		ChatObject chat = new ChatObject
			(ChatObject.ACTIVE_LIST, new ActiveUsers());
		
		ChatIntegrity.encryptAndSend((Serializable) chat,
			TaylorsChatClientProgram.chatClient.getClient());
	}	
	
	public void chatEventCall() {
		if (rejectIfNotAdmin()) return;
		
		if ((onlyOneUserIsSelected() && groupIsNotSelected()) ||
			(onlyOneGroupIsSelected() && userIsNotSelected())) {
			
			String recipient = onlyOneUserIsSelected() ?
				activeUsersList.getSelectedValue() :
					usersGroupsList.getSelectedValue();

			TaylorsChatClientProgram.currentRecipient = recipient;
			updateChatGui(recipient);
			switchToChatGui();

		} else {
			Util.promptError
				(new Exception
					("You can only choose one "
						+ "recipient (a user or a group)."));
		}
	}
	
	private boolean onlyOneUserIsSelected() {
		return (activeUsersList.getSelectedValuesList().size() == 1);
	}
	
	private boolean groupIsNotSelected() {
		return (usersGroupsList.getSelectedValuesList().isEmpty());
	}
	
	private boolean onlyOneGroupIsSelected() {
		return (usersGroupsList.getSelectedValuesList().size() == 1);
	}
	
	private boolean userIsNotSelected() {
		return (activeUsersList.getSelectedValuesList().isEmpty());
	}
	
	private void updateChatGui(String recipient) {
		ChatGUI chatGui = TaylorsChatClientProgram.gui.getChatGui();
		chatGui.getRecipientLabel().setText(recipient);
		chatGui.getOutputArea().setText(chatHistory.get(recipient));
	}
	
	private void switchToChatGui() {
		TaylorsChatClientProgram.gui.getTabPane().setSelectedIndex(0);		
	}
	
	public void usersGroupsActionsEventCall(String selectedAction) {
		if (selectedAction.equals(CREATE_NEW_GROUP)) {
			createNewUsersGroup();
		} else if (selectedAction.equals(EDIT_GROUP)) {
			editUsersGroup();
		} else if (selectedAction.equals(REMOVE_GROUP)) {
			removeUsersGroup();
		}
	}
	
	/**
	 * Create a new multicast group by specifying the
	 * group's name and select its members. Group's name
	 * can't be the same as any user's name.
	 */
	// TODO: Optimize this.
	private void createNewUsersGroup() {
		ArrayList<Component> components = new ArrayList<Component>();
		
		JList<String> activeUsersListClone = 
			new JList<String>(activeUsersList.getModel());
		
		ArrayList<String> memberList = new ArrayList<String>();
		
		components.add(activeUsersListClone);
		components.add(new JLabel("Users Group's Name:"));
		
		String groupName = JOptionPane.showInputDialog
			(null, components.toArray(),
				CREATE_NEW_GROUP, JOptionPane.INFORMATION_MESSAGE);
		
		ListModel<String> listModel = activeUsersList.getModel();
		
		for (int i = 0; i < listModel.getSize(); i++) {
			if (listModel.getElementAt(i).equals(groupName)) {
				Util.promptError
					(new Exception
						("Group's name can't be the same "
							+ "as any user's name."));
				
				return;
			}
		}
		
		memberList.addAll(activeUsersListClone.getSelectedValuesList());
		
		Object prevGroup = usersGroups
			.putIfAbsent(groupName, memberList);
		
		if (prevGroup != null)
			Util.promptError(new Exception("User group already exists."));
		else
			refreshUsersGroupsList();
	}
	
	/**
	 * Edit a multicast group by selecting different users
	 * from the list of active users.
	 */
	// TODO: Optimize this.
	private void editUsersGroup() {
		String groupName = usersGroupsList.getSelectedValue();
		
		if (groupName == null) return;

		JList<String> activeUsersListClone = 
			new JList<String>(activeUsersList.getModel());
		
		DefaultListModel<String> listModel = (DefaultListModel<String>)
				activeUsersListClone.getModel();
		
		activeUsersListClone.clearSelection();
		
		int[] selectedIndices = new int[listModel.size()];
		
		for (int i = 0; i < selectedIndices.length; i++)
			selectedIndices[i] = listModel.getSize();
		
		int j = 0;
		
		for (String member : usersGroups.get(groupName)) {

			for (int i = 0; i < listModel.getSize(); i++) {
				if (listModel.getElementAt(i).equals(member)) {
					selectedIndices[j] = i;
					break;
				}
			}
			
			j++;
		}

		activeUsersListClone.setSelectedIndices(selectedIndices);
		
		ArrayList<String> memberList = new ArrayList<String>();
		
		JOptionPane.showMessageDialog
			(null, activeUsersListClone,
				EDIT_GROUP, JOptionPane.INFORMATION_MESSAGE);
		
		memberList.addAll(activeUsersListClone.getSelectedValuesList());
		
		if (usersGroups.containsKey(groupName)) {
			usersGroups.put(groupName, memberList);
			
			refreshUsersGroupsList();
		}
	}
	
	/**
	 * Refresh multicast groups list every time a change
	 * is applied by the admin.
	 */
	private void refreshUsersGroupsList() {
		DefaultListModel<String> model = (DefaultListModel<String>)
			usersGroupsList.getModel();
		
		model.clear();
		
		for (String group : usersGroups.keySet())
			model.addElement(group);
	}
	
	private void removeUsersGroup() {
		for (String userGroup : usersGroupsList.getSelectedValuesList())
			usersGroups.remove(userGroup);
		
		refreshUsersGroupsList();
	}
	
	public void updateChatHistory(String username, String message) {
		String modifiedMsg = message.trim() + "\n";
		if (chatHistory.containsKey(username)) {
			chatHistory.put
				(username, chatHistory.get(username).concat(modifiedMsg));
		} else {
			chatHistory.put(username, modifiedMsg);
		}
	}
	
	public HashMap<String, String> getChatHistory() {
		return chatHistory;
	}

	public void setChatHistory(HashMap<String, String> chatHistory) {
		this.chatHistory = chatHistory;
	}
	
	public JPanel getCommunicationPanel() {
		return communicationPanel;
	}

	public void setCommunicationPanel(JPanel sessionsPanel) {
		this.communicationPanel = sessionsPanel;
	}

	public JList<String> getActiveUsersList() {
		return activeUsersList;
	}

	public void setActiveUsersList(JList<String> activeUsersList) {
		this.activeUsersList = activeUsersList;
	}
	
	public HashMap<String, ArrayList<String>> getUsersGroups() {
		return usersGroups;
	}
	
}