package gui;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.HashMap;

import javax.swing.*;

import main.TaylorsChatClientProgram;
import main.Util;
import voicechat.*;

public class ChatGUI implements ActionListener {
	
	/**
	 * This class displays a user interface for clients
	 * to chat with each other and also contains LoginGUI.
	 */
	
	private static final String TAYLORS_LOGO_PATH =
		"/images/taylors-logo.png";
	
	private LoginGUI loginGUI;
	
	private JPanel chatPanel;
	private JPanel recipientPanel;
	private JLabel recipientLbl;
	
	private JScrollPane scrollPane;
	private JTextArea outputArea;
	
	private JPanel inputPanel;
	private JTextArea inputArea;
	
	private JPanel chatActionsPanel;
	private JCheckBox sendBroadcastCheckBox;
	private JButton sendMsgBtn;
	
	private VoiceRecorder recorder;
	private JButton recordVoiceBtn;

	private JPanel footerPanel;
	private JLabel taylorsLogoLbl;
	private JLabel recordingLbl;
	
	public ChatGUI() {
		loginGUI = new LoginGUI();
		
		chatPanel = new JPanel();
		recipientPanel = new JPanel();
		recipientLbl = new JLabel("");
		
		outputArea = new JTextArea(100, 100);
		
		inputPanel = new JPanel();
		inputArea = new JTextArea(30, 100);
		
		chatActionsPanel = new JPanel();
		sendBroadcastCheckBox = new JCheckBox("Broadcast");
		sendMsgBtn = new JButton("Send");
		
		recorder = new VoiceRecorder();
		recordVoiceBtn = new JButton("Record");
		
		footerPanel = new JPanel();
		ImageIcon taylorsLogo = getResizedTaylorsLogo();
		taylorsLogoLbl = new JLabel(taylorsLogo);
		recordingLbl = new JLabel("");
	}
	
	private ImageIcon getResizedTaylorsLogo() {
		ImageIcon taylorsLogo = new ImageIcon
			(getClass().getResource(TAYLORS_LOGO_PATH));

		Image img = taylorsLogo.getImage();
		
		img = img.getScaledInstance(95, 27, Image.SCALE_SMOOTH);
		return new ImageIcon(img);
	}

	public void init() {
		chatPanel.setLayout(new BoxLayout(chatPanel, BoxLayout.PAGE_AXIS));
		
		initLoginGUI();
		initRecipientPanel();
		initOutputArea();
		initInputPanel();
		initFooterPanel();
	}
	
	private void initLoginGUI() {
		loginGUI.init();
		JPanel loginPanel = loginGUI.getLoginPanel();
		chatPanel.add(loginPanel);
	}
	
	private void initRecipientPanel() {
		recipientPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		recipientLbl.setFont(new Font("Dialog", Font.PLAIN, 20));
		recipientPanel.add(recipientLbl);
		chatPanel.add(recipientPanel);		
	}
	
	private void initOutputArea() {
		outputArea.setEditable(false);
		outputArea.setFont(new Font("Dialog", Font.PLAIN, 16));
		outputArea.setBackground(new Color(240, 240, 255));
		outputArea.setLineWrap(true);
		scrollPane = getScrollPaneOf(outputArea);
		chatPanel.add(scrollPane);
	}
	
	private JScrollPane getScrollPaneOf(JTextArea textArea) {
		return new JScrollPane
			(textArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	}
	
	private void initInputPanel() {
		inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.LINE_AXIS));
		initInputArea();
		initChatActionsPanel();
		chatPanel.add(inputPanel);		
	}
	
	private void initInputArea() {
		inputArea.setFont(new Font("Dialog", Font.PLAIN, 16));
		inputArea.setLineWrap(true);
		scrollPane = getScrollPaneOf(inputArea);
		inputPanel.add(scrollPane);		
	}
	
	private void initChatActionsPanel() {
		chatActionsPanel.setLayout(new BorderLayout(0, 0));
		
		sendBroadcastCheckBox.setVisible(false);
		chatActionsPanel.add(sendBroadcastCheckBox, BorderLayout.NORTH);
		
		sendMsgBtn.addActionListener(this);
		chatActionsPanel.add(sendMsgBtn, BorderLayout.CENTER);
		
		recordVoiceBtn.addActionListener(this);
		chatActionsPanel.add(recordVoiceBtn, BorderLayout.SOUTH);
		
		inputPanel.add(chatActionsPanel);
	}
	
	private void initFooterPanel() {
		footerPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		footerPanel.add(taylorsLogoLbl);
		footerPanel.add(recordingLbl);
		chatPanel.add(footerPanel);		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == sendMsgBtn) {
			sendMsgEventCall();
		} else if (e.getSource() == recordVoiceBtn) {
			recordVoiceEventCall();
		}
	}
	
	/**
	 * Send either a text message or a voice message
	 * depends on the content in inputArea.
	 */
	public void sendMsgEventCall() {
		if (inputArea.getText().trim().isEmpty()) return;

		if (TaylorsChatClientProgram.chatClient
			.getUsername().isEmpty()) return;

		if (TaylorsChatClientProgram.currentRecipient.isEmpty()) return;
		
		String recipient = TaylorsChatClientProgram.currentRecipient;
		
		try {
			if (inputArea.getText().equals(recorder.getRecordName())) {
				sendVoiceChatMessage();
				inputArea.setText("");
			} else {
				TaylorsChatClientProgram.chatClient
					.sendChatMessage(recipient, inputArea.getText());
	
				inputArea.setText("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Get the recipient IP address from the server,
	 * and send a voice chat message to him/her.
	 */
	private void sendVoiceChatMessage() {
		File voiceFile = recorder.getRecordFile();
		String recipientIP = 
			TaylorsChatClientProgram.chatClient
				.getRecipientIP(TaylorsChatClientProgram.currentRecipient);
		
		if (recipientIP.isEmpty()) {
			Util.promptError
				(new Exception
					("Failed fetching recipient's IP address from Server,"
						+ "\nPlease try again")); 
			return;
		}

		try {
			VoiceSender.sendVoiceChat(voiceFile, recipientIP);
		} catch (Exception e) {
			e.printStackTrace();
			Util.promptError(e);
		}
	}
	
	/**
	 * Record or stop recording depends on the state
	 * of the voice recorder.
	 */
	public void recordVoiceEventCall() {
		if (TaylorsChatClientProgram.chatClient
			.getUsername().isEmpty()) return;
		if (TaylorsChatClientProgram.currentRecipient.isEmpty()) return;
		
		RecordHandler recordHandler = new RecordHandler(recorder);
		
		recordHandler.start();
		waitFlagSettingBeforeUpdate();
		toggleRecordBtnAndLbl();
		
		if (recordHandler.isSuccessful()) {

			inputArea.setText(recorder.getRecordName());

		} else if (! recorder.isRecording() &&
				! recordHandler.isSuccessful()) {

			Util.promptError
				(new Exception
					("Something wrong happened while recording!"));
		}
	}
	
	/**
	 * Sleep the caller thread, wait for record handler
	 * to set whether the microphone is recording or not
	 * so other component can used it
	 */
	private void waitFlagSettingBeforeUpdate() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Update the GUI for voice chat depends on
	 * the state of voice recorder.
	 */
	private void toggleRecordBtnAndLbl() {
		boolean isRecording = recorder.isRecording();
		
		recordingLbl.setText(isRecording ? "Recording..." : "Stopped.");
		recordVoiceBtn.setText(isRecording ? "Stop" : "Record");
		sendMsgBtn.setEnabled(! isRecording);
	}
	
	public void updateOutputArea(String username) {
		HashMap<String, String> chatHistory = 
			TaylorsChatClientProgram.gui
				.getCommunicationGui().getChatHistory();

		TaylorsChatClientProgram.gui.getChatGui()
			.getOutputArea().setText(chatHistory.get(username));		
	}
	
	public JPanel getChatPanel() {
		return chatPanel;
	}
	
	public JLabel getRecipientLabel() {
		return recipientLbl;
	}

	public JTextArea getOutputArea() {
		return outputArea;
	}

	public void setOutputArea(JTextArea outputArea) {
		this.outputArea = outputArea;
	}

	public JTextArea getInputArea() {
		return inputArea;
	}

	public void setInputArea(JTextArea inputArea) {
		this.inputArea = inputArea;
	}
	
	public JCheckBox getSendBroadcastCheckBox() {
		return sendBroadcastCheckBox;
	}
	
}