package gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import main.TaylorsChatClientProgram;
import objects.*;

public class LoginGUI implements ActionListener {
	
	/**
	 * This class displays a user interface for signing
	 * in. It communicates with ChatClient to start
	 * the receiving thread if login is successful.
	 */
	
	private JPanel loginPanel;
	private JLabel usernameLbl;
	private JLabel passwordLbl;
	private JTextField usernameField;
	private JPasswordField passwordField;
	private JButton loginBtn;
	
	public LoginGUI() {
		loginPanel = new JPanel();
		usernameLbl = new JLabel("Username: ");
		passwordLbl = new JLabel("Password: ");
		usernameField = new JTextField("", 10);
		passwordField = new JPasswordField("", 10);
		loginBtn = new JButton("Login");
	}
	
	public void init() {
		loginPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 20));
		
		usernameField.addActionListener(this);
		passwordField.addActionListener(this);
		loginBtn.addActionListener(this);
		
		loginPanel.add(usernameLbl);
		loginPanel.add(usernameField);
		loginPanel.add(passwordLbl);
		loginPanel.add(passwordField);
		loginPanel.add(loginBtn);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if ((e.getSource() == loginBtn) ||
			(e.getSource() == usernameField) ||
			(e.getSource() == passwordField)) {

			// If login is successful, start chat client thread.
			if (loginEventCall())
				TaylorsChatClientProgram.chatClient.start();
		}
	}
	
	public boolean loginEventCall() {
		String username = usernameField.getText();
		String password = new String(passwordField.getPassword());
		
		try {
			return callUserLoginIfValid(username, password);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private boolean callUserLoginIfValid
		(String username, String password) throws Exception {

		if (username.isEmpty() || password.isEmpty()) {
			JOptionPane.showMessageDialog
			(
					null, 
					"Please provide username and password.", 
					"Not Enough Information", 
					JOptionPane.ERROR_MESSAGE
			);
			return false;
		}

		if (TaylorsChatClientProgram.chatClient == null)
			return false;
		
		ChatObject result = TaylorsChatClientProgram
			.chatClient.userLogin(username, password);

		return checkLoginResultAndRespondAccordingly(result);
	}
	
	private boolean checkLoginResultAndRespondAccordingly
		(ChatObject result) {

		if (result.getType() == ChatObject.SERVER_ERROR) {
			displayInvalidLoginResponse(result);
			return false;
		} else {
			displaySuccessfulLoginResponse();
			updateGuiAfterLogin();
			return true;
		}		
	}
	
	private void displayInvalidLoginResponse(ChatObject result) {
		ServerError error = (ServerError) result.getObj();
		JOptionPane.showMessageDialog
		(
				null, 
				error.getMsg(), 
				"Invalid Login", 
				JOptionPane.ERROR_MESSAGE
		);		
	}
	
	private void displaySuccessfulLoginResponse() {
		JOptionPane.showMessageDialog
		(
				null, 
				"Successfully logged in . Enjoy!", 
				"Login", 
				JOptionPane.INFORMATION_MESSAGE
		);		
	}
	
	private void updateGuiAfterLogin() {
		loginPanel.setVisible(false);
		
		String username = TaylorsChatClientProgram.chatClient
			.getUsername().toLowerCase();

		if (username.equals("admin")) {
			TaylorsChatClientProgram.gui.addActiveUsersTab();
			TaylorsChatClientProgram.gui.addBroadcastCheckBox();
			TaylorsChatClientProgram.gui.addAdministrationTab();
		} else {
			TaylorsChatClientProgram.currentRecipient = "admin";
		}
		
		TaylorsChatClientProgram.gui
			.getChatGui().getRecipientLabel()
				.setText(TaylorsChatClientProgram.currentRecipient);		
	}
	
	public JPanel getLoginPanel() {
		return loginPanel;
	}
	
}