package gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.util.*;
import java.util.Map.Entry;

import javax.swing.*;
import javax.swing.border.EtchedBorder;

import administrations.*;
import main.*;

public class AdministrationGUI implements ActionListener {

	/**
	 * This class displays a user interface for admin
	 * to manage the users remotely.
	 */
	
	private static final int REG_PORT = 1099;
	
	private Registry reg;
	private UserAdministrationInterface administration;
	
	private JPanel adminPanel, userDetailsPanel, functionsPanel;
	private JTextField usernameField;
	private HashMap<String, JButton> buttons;
	
	public AdministrationGUI() {
		adminPanel = new JPanel();
		
		userDetailsPanel = new JPanel();
		usernameField = new JTextField(15);
		
		functionsPanel = new JPanel();
		
		buttons = new HashMap<String, JButton>();
		buttons.put("changePasswordBtn", new JButton("Change Password"));
		buttons.put("kickUserBtn", new JButton("Kick User"));
		buttons.put("banUserBtn", new JButton("Ban User"));
		buttons.put("unbanUserBtn", new JButton("Unban User"));
		buttons.put("queryUsers", new JButton("Query All Users"));
		buttons.put("addUserBtn", new JButton("Add New User"));
		buttons.put("removeUserBtn", new JButton("Remove User"));
	}
	
	public void init() {
		try {
			
			reg = LocateRegistry.getRegistry
				(TaylorsChatClientProgram.serverIP, REG_PORT);

			administration = (UserAdministrationInterface) 
				reg.lookup("UserAdministration");
			
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
			Util.promptError(e);
		}
		
		adminPanel.setLayout(new BorderLayout());
		
		initUserDetailsPanel();
		initFunctionsPanel();
	}
	
	public void initUserDetailsPanel() {
		userDetailsPanel.setLayout(new FlowLayout());
		
		JLabel usernameLbl = new JLabel("Username:");
		userDetailsPanel.add(usernameLbl);
		userDetailsPanel.add(usernameField);
		
		userDetailsPanel.setPreferredSize
			(new Dimension(MainGUI.WINDOW_WIDTH, 40));
		
		adminPanel.add(userDetailsPanel, BorderLayout.NORTH);
	}
	
	private void initFunctionsPanel() {
		functionsPanel.setLayout(new GridLayout(0, 3));
		functionsPanel.setBorder
			(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));

		initButtonsPanels();
		
		functionsPanel.setPreferredSize
			(new Dimension(MainGUI.WINDOW_WIDTH, 360));
		
		adminPanel.add(functionsPanel, BorderLayout.SOUTH);
	}
	
	private void initButtonsPanels() {
		for (Entry<String, JButton> entry : buttons.entrySet()) {
			JPanel panel = new JPanel();
			JButton button = entry.getValue();
			
			panel.setLayout(new BorderLayout());
			
			button.addActionListener(this);
			panel.add(button, BorderLayout.CENTER);
			
			functionsPanel.add(panel);
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (reg == null || administration == null) return;
		
		try {
			if (e.getSource() == buttons.get("changePasswordBtn")) {
				changePasswordEventCall();
			} else if (e.getSource() == buttons.get("kickUserBtn")) {
				kickUserEventCall();
			} else if (e.getSource() == buttons.get("banUserBtn")) {
				banUserEventCall();
			} else if (e.getSource() == buttons.get("unbanUserBtn")) {
				unbanUserEventCall();
			} else if (e.getSource() == buttons.get("queryUsers")) {
				queryUsersEventCall();
			} else if (e.getSource() == buttons.get("addUserBtn")) {
				addUserEventCall();
			} else if (e.getSource() == buttons.get("removeUserBtn")) {
				removeUserEventCall();
			}
		} catch (RemoteException re) {
			re.printStackTrace();
			Util.promptError(re);
		}
	}
	
	private void changePasswordEventCall() throws RemoteException {
		if (usernameField.getText().isEmpty()) return;
		
		String password = askForUserPassword("Change Password");
		
		if (password.isEmpty()) return;
		
		ActionResponse res = administration.changeUserPassword
			(usernameField.getText(), password);
		
		Util.promptResult(res);
	}
	
	/**
	 * Show an input box asking for the new user's password.
	 * This input box is shown every time the admin tries
	 * to add a new user or change user's password.
	 * 
	 * @param title
	 * @return new user's password
	 */
	private String askForUserPassword(String title) {
		ArrayList<Component> components = new ArrayList<Component>();
		JPasswordField passwordField = new JPasswordField(15);

		components.add(new JLabel("User's Password:"));
		components.add(passwordField);
		
		JOptionPane.showMessageDialog
			(null, components.toArray(),
				title, JOptionPane.INFORMATION_MESSAGE);
		
		return new String(passwordField.getPassword());
	}
	
	private void kickUserEventCall() throws RemoteException {
		if (usernameField.getText().isEmpty()) return;
		
		ActionResponse res = administration.kickUser(usernameField.getText());
		
		Util.promptResult(res);
	}
	
	private void banUserEventCall() throws RemoteException {
		if (usernameField.getText().isEmpty()) return;
		
		ActionResponse res = administration.banUser(usernameField.getText());
		
		Util.promptResult(res);
	}
	
	private void unbanUserEventCall() throws RemoteException {
		if (usernameField.getText().isEmpty()) return;
		
		ActionResponse res = administration.unbanUser(usernameField.getText());
		
		Util.promptResult(res);
	}
	
	/**
	 * Get all usernames from the database and write them to users.txt.
	 * 
	 * @throws RemoteException
	 */
	private void queryUsersEventCall() throws RemoteException {
		String[] users = administration.queryAllUsers();
		String fileName = "users.txt";
		File file = new File(fileName);
		PrintWriter writer;
		
		try {
			writer = new PrintWriter(file);
			
			for (String user : users)
				writer.println(user);
			
			writer.close();
			
			ActionResponse res = new ActionResponse
				(ActionResponse.ACTION_SUCCESS,
					users.length + " usernames have been written into " + fileName);
			
			Util.promptResult(res);
		} catch (IOException e) {
			e.printStackTrace();
			Util.promptError(e);
		}
	}
	
	private void addUserEventCall() throws RemoteException {
		if (usernameField.getText().isEmpty()) return;
		
		String password = askForUserPassword("Add New User");
		
		if (password.isEmpty()) return;
		
		ActionResponse res = administration.addUserToDB
			(usernameField.getText(), password);
		
		Util.promptResult(res);
	}
	
	private void removeUserEventCall() throws RemoteException {
		if (usernameField.getText().isEmpty()) return;
		
		ActionResponse res = administration
			.removeUserFromDB(usernameField.getText());
		
		Util.promptResult(res);
	}
	
	public JPanel getAdminPanel() {
		return adminPanel;
	}
	
}