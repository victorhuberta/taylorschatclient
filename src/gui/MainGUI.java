package gui;

import java.awt.*;
import javax.swing.*;

public class MainGUI extends JFrame {
	
	/**
	 * This class manages all other child GUIs and
	 * acts as their parents.
	 */
	
	private static final long serialVersionUID = 1L;
	
	public static final String DEFAULT_TITLE =
		"Taylor's University: Chat With Admin!";
	public static final int WINDOW_WIDTH = 640;
	public static final int WINDOW_HEIGHT = 480;
	public static final int WINDOW_MIN_WIDTH = 640;
	public static final int WINDOW_MIN_HEIGHT = 480;
	public static final Color taylorsRedColor = new Color(180, 0, 0);
	
	private JPanel mainPanel;
	private JTabbedPane tabPane;
	private ChatGUI chatGui;
	private CommunicationGUI communicationGui;
	private AdministrationGUI administrationGui;
	private JPanel chatPanel;
	private JPanel communicationPanel;
	private JPanel adminPanel;
	
	public MainGUI() {
		mainPanel = new JPanel();
		tabPane = new JTabbedPane();
		
		chatGui = new ChatGUI();
		communicationGui = new CommunicationGUI();
		administrationGui = new AdministrationGUI();
		
		chatPanel = chatGui.getChatPanel();
		communicationPanel = communicationGui.getCommunicationPanel();
		adminPanel = administrationGui.getAdminPanel();
	}

	public void init() {
		initMainPanel();
		setTitle(DEFAULT_TITLE);
		
		setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
		setMinimumSize(new Dimension(WINDOW_MIN_WIDTH, WINDOW_MIN_HEIGHT));
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		pack();
		setVisible(true);		
	}
	
	private void initMainPanel() {
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.setBackground(taylorsRedColor);
		
		chatGui.init();
		communicationGui.init();
		administrationGui.init();
		
		initTabPane();
		add(mainPanel);
	}
	
	private void initTabPane() {
		tabPane.addTab("Chat", null, chatPanel, "Have a chat session");
		mainPanel.add(tabPane);
	}
	
	public void addActiveUsersTab() {
		tabPane.addTab
			("Active Users", null, communicationPanel,
				"Show a list of current active users");
	}
	
	public void addBroadcastCheckBox() {
		chatGui.getSendBroadcastCheckBox().setVisible(true);
	}
	
	public void addAdministrationTab() {
		tabPane.addTab("Administration", null,
			adminPanel, "Manage the users remotely");
	}

	public JTabbedPane getTabPane() {
		return tabPane;
	}

	public void setTabPane(JTabbedPane tabPane) {
		this.tabPane = tabPane;
	}

	public ChatGUI getChatGui() {
		return chatGui;
	}

	public void setChatGui(ChatGUI chatGui) {
		this.chatGui = chatGui;
	}

	public CommunicationGUI getCommunicationGui() {
		return communicationGui;
	}

	public void setCommunicationGui(CommunicationGUI sessionsGui) {
		this.communicationGui = sessionsGui;
	}
	
}