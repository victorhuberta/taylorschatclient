package objects;

import java.io.Serializable;
import java.util.ArrayList;

public class RecipientGroup implements Serializable {
	
	/**
	 * Specify which users are members of a multicast
	 * group.
	 */
	
	private static final long serialVersionUID = 1L;

	private String groupName;
	private ArrayList<String> members = new ArrayList<>();
	
    public RecipientGroup(String groupName) {
    	this.groupName = groupName;
    }

    public void addMember(String memberName){
    	members.add(memberName);
    }

    public void setGroupName(String groupName){
    	this.groupName = groupName;
    }
    
    public String getGroupName(){
    	return groupName;
    }
    
    public ArrayList<String> getMembers(){
    	return members;
    }
    
    public void setMembers(ArrayList<String> members) {
    	this.members = members;
    }
	
}