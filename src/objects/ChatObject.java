package objects;

import java.io.Serializable;
import java.util.Date;

public class ChatObject implements Serializable {
	
	/**
	 * This class specifies some information on the
	 * packet exchanged by client and server, including
	 * authentication token, created date and time, 
	 * packet type, and object that this packet stores.
	 * 
	 * This class is able to store several types of
	 * object:
	 * 1. Credentials object
	 * 2. ChatMessage object
	 * 3. ActiveUsers object
	 * 4. ServerError object
	 * 5. RecipientGroup object
	 * 6. A plain String object (for RECIPIENT_IP type)
	 */

	private static final long serialVersionUID = 1L;
	
	public static final int AUTH = 333;
	public static final int MSG = 444;
	public static final int ACTIVE_LIST = 555;
	public static final int SERVER_ERROR = 666;
    public static final int BROADCAST = 888;
    public static final int MULTICAST = 999;
    public static final int RECIPIENT_IP = 1000;
	
	private String authToken;
	private String dateTime;
	private int type;
	private Object obj;
	
	private RecipientGroup recipientGroup;
	
	public ChatObject(int type, Object obj) {
		authToken = "";
		dateTime = new Date().toString();
		this.type = type;
		this.obj = obj;
	}

	public ChatObject reset(Object[] argv) {
		authToken = "";
		dateTime = new Date().toString();
		
		if (argv != null && argv.length == 2) {
			type = (Integer) argv[0];
			obj = argv[1];
		}
		
		return this;
	}
	
	public String getAuthToken() {
		return authToken;
	}
	
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getDateTime() {
		return dateTime;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}
	
	public RecipientGroup getRecipientGroup() {
		return recipientGroup;
	}
	
	public void setRecipientGroup(RecipientGroup recipientGroup) {
		this.recipientGroup = recipientGroup;
	}

}