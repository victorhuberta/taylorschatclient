package objects;

import java.io.Serializable;
import java.util.HashSet;

public class ActiveUsers implements Serializable {
	
	/**
	 * This class stores a unique set of all active users.
	 */

	private static final long serialVersionUID = 1L;
	
	private HashSet<String> activeUsersSet;
	
	public ActiveUsers() {
		activeUsersSet = new HashSet<String>();
	}

	public HashSet<String> getActiveUsersSet() {
		return activeUsersSet;
	}

	public void setActiveUsersSet(HashSet<String> activeUsersSet) {
		this.activeUsersSet = activeUsersSet;
	}
	
}