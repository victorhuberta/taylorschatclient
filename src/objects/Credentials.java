package objects;

import java.io.Serializable;

public class Credentials implements Serializable {

	/**
	 * This class stores the username and password
	 * of client.
	 */
	
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	
	public Credentials() {
		username = "";
		password = "";
	}
	
	public Credentials(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
		return;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
		return;
	}
	
}