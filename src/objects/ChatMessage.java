package objects;

import java.io.Serializable;

public class ChatMessage implements Serializable {
	
	/**
	 * This class specifies some information about
	 * a chat message, including sender name, recipient
	 * name, and message text.
	 */
	
	private static final long serialVersionUID = 1L;
	
	private String sender;
	private String recipient;
	private String message;
	
	public ChatMessage(String recipient, String message) {
		this.recipient = recipient;
		this.message = message;
	}
	
	public String getSender() {
		return sender;
	}
	
	public void setSender(String sender) {
		this.sender = sender;
	}
	
	public String getRecipient() {
		return recipient;
	}
	
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}