package objects;

import java.io.Serializable;

public class ServerError implements Serializable {
	
	/**
	 * This class stores different types of error
	 * code and message, including:
	 * 1. Unknown error
	 * 2. Credentials error
	 * 3. Authentication token error
	 * 4. Inactive user error
	 * 5. Invalid chat object type error
	 * 6. Banned from log in error
	 */
	
	private static final long serialVersionUID = 1L;
	
	public static final int ERROR_UNKNOWN = 456;
	public static final int ERROR_CREDENTIALS = 789;
	public static final int ERROR_AUTH_TOKEN = 102;
	public static final int ERROR_NOT_ACTIVE = 345;
	public static final int ERROR_INVALID_TYPE = 678;
	public static final int ERROR_BANNED = 901;
	
	private int code;
	private String msg;
	
	public ServerError(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}