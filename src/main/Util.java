package main;

import javax.swing.JOptionPane;

import administrations.ActionResponse;

public class Util {
	
	/**
	 * A class that provides several utility
	 * methods for other classes to use.
	 */
	
	public static void promptError(Exception e) {
		JOptionPane.showMessageDialog
			(null, e.getMessage(),
				"Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public static void promptResult(ActionResponse res) {
		JOptionPane.showMessageDialog
			(null, res.getMsg(),
				"Action Result", JOptionPane.INFORMATION_MESSAGE);
	}
	
}