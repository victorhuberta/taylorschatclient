package main;

import java.io.*;
import java.net.Socket;
import javax.swing.*;

import gui.CommunicationGUI;
import objects.*;
import security.*;

public class ChatClient extends Thread {

	/**
	 * This class handles the interaction between client
	 * and server. It sends and receives messages by using
	 * ChatIntegrity encryption and decryption features, 
	 * as well as updating GUI components to reflect the 
	 * current state of client.
	 * 
	 * Note: to test this class, you need to change
	 * the server's IP address manually.
	 */
	
	private String serverIP = "127.0.0.1";
	private final int serverPort = 4444;
	
	private String username;
	private Socket client;
	private ChatObject chat;
	
	// To store the recipient's IP address object,
	// reset to null every time it is used.
	private ChatObject recipientIPChatObject;
	
	public ChatClient(String serverIP) throws IOException {
		this.serverIP = serverIP;
		username = "";
		client = new Socket(serverIP, serverPort);
	}
	
	@Override 
	public void run() {
		try {
			while (true) receiveChatObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ChatObject userLogin(String username, String password)
		throws Exception {

		Credentials creds = new Credentials(username, password);
		chat = new ChatObject(ChatObject.AUTH, creds);
		
		ChatIntegrity.encryptAndSend((Serializable) chat, client);
		ChatObject response = (ChatObject) ChatIntegrity.recvAndDecrypt(client);
		
		// If successfully logged in, replace chat object with new one.
		if (response.getType() != ChatObject.SERVER_ERROR) {
			chat = response;
			this.username = username;
		}
		
		return response;
	}
	
	public void sendChatMessage(String recipient, String text)
		throws Exception {

		if (chat == null) return;
		if (username.isEmpty()) return;
		
		prepareChatObjectWithProperMessage(recipient, text);
		ChatIntegrity.encryptAndSend((Serializable) chat, client);
		updateChatHistoryAndOutputArea(recipient, text, true);
	}
	
	/**
	 * Prepare a chat object to be sent, depends on
	 * whether or not the broadcast checkbox is selected,
	 * and a multicast group is selected.
	 * 
	 * @param recipient
	 * @param text
	 */
	private void prepareChatObjectWithProperMessage
		(String recipient, String text) {

		CommunicationGUI commGui = TaylorsChatClientProgram
			.gui.getCommunicationGui();
		
		ChatMessage msg = new ChatMessage(recipient, text);
		msg.setSender(username);
		
		if (TaylorsChatClientProgram.gui
				.getChatGui().getSendBroadcastCheckBox().isSelected()) {

			chat.setType(ChatObject.BROADCAST);

		} else if (commGui.getUsersGroups().get(recipient) != null) {
			chat.setType(ChatObject.MULTICAST);

			RecipientGroup rg = new RecipientGroup(recipient);
			rg.setMembers(commGui.getUsersGroups().get(recipient));

			chat.setRecipientGroup(rg);
		} else {
			chat.setType(ChatObject.MSG);
		}
		
		chat.setObj(msg);
	}
	
	private void updateChatHistoryAndOutputArea
		(String chatPartner, String text, boolean isSending) {

		text = isSending ?
			(username + ": " + text) :
				(chatPartner + ": " + text); 

		TaylorsChatClientProgram.gui
			.getCommunicationGui()
				.updateChatHistory(chatPartner, text);
		TaylorsChatClientProgram.gui
			.getChatGui().updateOutputArea(chatPartner);
	}
	
	public void receiveChatObject() throws Exception {
		if (chat == null) return;

		ChatObject chat = (ChatObject)
			ChatIntegrity.recvAndDecrypt(client);
		
		switch (chat.getType()) {
		case ChatObject.MSG:
			updateGuiForNewChatMessage(chat);
			break;
		case ChatObject.ACTIVE_LIST:
			updateActiveUsersList(chat);
			break;
		case ChatObject.SERVER_ERROR:
			displayErrorResponse(chat);
			break;
		case ChatObject.RECIPIENT_IP:
			setRecipientIPChatObject(chat);
			break;
		default:
		}
	}
	
	private void updateGuiForNewChatMessage(ChatObject chat) {
		ChatMessage msg = (ChatMessage) chat.getObj();

		updateChatHistoryAndOutputArea
			(msg.getSender(), msg.getMessage(), false);

		TaylorsChatClientProgram
			.currentRecipient = msg.getSender();

		TaylorsChatClientProgram.gui.getChatGui()
			.getRecipientLabel().setText(msg.getSender());		
	}
	
	private void updateActiveUsersList(ChatObject chat) {
		JList<String> activeUsersList = 
			TaylorsChatClientProgram.gui
				.getCommunicationGui().getActiveUsersList();

		DefaultListModel<String> activeUsersListModel =
			(DefaultListModel<String>) activeUsersList.getModel();
		
		activeUsersListModel.clear();
		
		ActiveUsers activeUsers = (ActiveUsers) chat.getObj();

		for (Object activeUser : activeUsers
			.getActiveUsersSet().toArray()) {

			if (activeUser == null) continue;
			if (((String) activeUser).equals("admin")) continue;

			activeUsersListModel.addElement((String) activeUser);
		}
	}
	

	private void displayErrorResponse(ChatObject chat) {
		ServerError response = (ServerError) chat.getObj();

		JOptionPane.showMessageDialog
			(null, response.getMsg(),
				"Error", JOptionPane.ERROR_MESSAGE);		
	}
	
	/**
	 * Get the recipient's IP address for the peer
	 * -to-peer voice chat.
	 * 
	 * @param recipient
	 * @return recipient's IP address
	 */
	public String getRecipientIP(String recipient) {
		ChatObject chat = new ChatObject
			(ChatObject.RECIPIENT_IP, recipient);
		String recipientIP = "";
		
		try {
			ChatIntegrity.encryptAndSend((Serializable) chat, client);

			chat = recipientIPChatObject;
			setRecipientIPChatObject(null);
			
			if (chat.getType() == ChatObject.SERVER_ERROR) {
				ServerError error = (ServerError) chat.getObj();
				Util.promptError(new Exception(error.getMsg()));
			} else {
				recipientIP = (String) chat.getObj();
			}
		} catch (NullPointerException e) {
			// Ignore nulls
		} catch (Exception e) {
			Util.promptError(e);
		}
		
		return recipientIP;
	}
	
	public String getUsername() {
		return username;
	}

	public Socket getClient() {
		return client;
	}

	public void setClient(Socket client) {
		this.client = client;
	}

	public String getServerIP() {
		return serverIP;
	}

	public int getServerPort() {
		return serverPort;
	}
	
	private synchronized void setRecipientIPChatObject
		(ChatObject recipientIPChatObject) {

		this.recipientIPChatObject = recipientIPChatObject;
	}

}