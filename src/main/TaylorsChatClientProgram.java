/*******************************************
 * Authors: 
 *   - Victor Huberta
 *   - Daniel Jonathan
 *   - Muhammad Shodiq Sabelputra
 * 
 * Title: Taylor's Chat Client
 * Date: June 24th, 2016
 *******************************************/

package main;

import java.io.IOException;

import javax.swing.JOptionPane;

import gui.*;
import voicechat.VoiceReceiver;

public class TaylorsChatClientProgram {
	
	/**
	 * This class acts as the manager of application's GUI
	 * and chat client. Many methods from both sides reference
	 * variables declared in this class.
	 */
	
	public static MainGUI gui;
	public static ChatClient chatClient;
	public static String currentRecipient = "";
	
	public static String serverIP = "localhost";
	
	public static void main(String[] args) {
		// Need to ask the server IP address from user
		serverIP = JOptionPane.showInputDialog
			(
				null, 
				"Please provide the server IP address:", 
				"Server IP Address Needed", 
				JOptionPane.INFORMATION_MESSAGE
			);
		if (serverIP == null || serverIP.isEmpty()) System.exit(0);
		
		gui = new MainGUI();
		gui.init();
		try {
			chatClient = new ChatClient(serverIP);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Start the VoiceReceiver server to listen
		// for voice messages.
		VoiceReceiver voiceReceiver = new VoiceReceiver();
		voiceReceiver.start();
	}
}