package administrations;

import java.rmi.*;

public interface UserAdministrationInterface extends Remote {
	
	public ActionResponse kickUser(String username) throws RemoteException;
	public ActionResponse banUser(String username) throws RemoteException;
	public ActionResponse unbanUser(String username) throws RemoteException;
	
	public ActionResponse changeUserPassword(String username, String newPassword)
		throws RemoteException;

	public String[] queryAllUsers() throws RemoteException;

	public ActionResponse addUserToDB(String username, String password)
		throws RemoteException;
	public ActionResponse removeUserFromDB(String username)
		throws RemoteException;
	
}