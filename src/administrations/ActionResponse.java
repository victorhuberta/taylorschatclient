package administrations;

import java.io.Serializable;

public class ActionResponse implements Serializable {
	
	/**
	 * Response sent by the server every time a
	 * remote method is invoked by the client.
	 * 
	 * The result is either:
	 * ACTION_SUCCESS or ACTION_FAILURE.
	 */
	
	private static final long serialVersionUID = 1L;
	
	public static final int ACTION_SUCCESS = 1;
	public static final int ACTION_FAILURE = -1;
	
	private int type;
	private String msg;
	
	public ActionResponse(int type, String msg) {
		this.type = type;
		this.msg = msg;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}