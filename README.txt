Before running the TaylorsChatClient.jar,
please do install Java Cryptography Extension (JCE) Unlimited
Strength Jurisdiction Policy 8. JCE policy is needed for encryption
and decryption purposes by the system.

Steps:
1. Extract jce_policy-8.zip into any folder.
2. Inside the jce/ folder, copy all *.jar files into
${java.home}/jre/lib/security/.
3. You are done!

If you are testing all features of the system (both client & server),
you need at least three different machines to avoid IP address
& port problems.

Now you should be able to start the programs just fine.
Notify me on [victorhuberta@gmail.com] if anything goes wrong!
